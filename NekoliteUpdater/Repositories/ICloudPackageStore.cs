﻿using NekoliteUpdater.Models;

namespace NekoliteUpdater.Repositories
{
    public interface ICloudPackageStore
    {
        Task<IReleaseVersion?> GetLatestVersionAsync(string? url, CancellationToken token);

        Task<Stream> GetReleaseStreamAsync(IReleaseVersion version, CancellationToken token);

    }
}

