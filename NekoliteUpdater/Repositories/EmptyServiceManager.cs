﻿namespace NekoliteUpdater.Repositories
{
    internal class EmptyServiceManager : IServiceManager
    {

        public Task StartServiceAsync(CancellationToken token)
        {
            return Task.CompletedTask;
        }

        public Task StopServiceAsync(CancellationToken token)
        {
            return Task.CompletedTask;
        }
    }
}

