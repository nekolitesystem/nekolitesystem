﻿namespace NekoliteUpdater.Repositories
{
    public interface IServiceManager
    {
        Task StopServiceAsync(CancellationToken token);

        Task StartServiceAsync(CancellationToken token);
    }
}

