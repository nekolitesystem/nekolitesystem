﻿namespace NekoliteUpdater.Repositories
{
    public interface ILocalPackageStore
    {
        Task<string?> GetVersionAsync(CancellationToken token);
        Task ExtractAsync(Stream stream, CancellationToken token);
        ValueTask OverwriteAsync(CancellationToken token);
        bool ExistUpdate();
    }
}
