﻿using NekoliteUpdater.Models;
using System.Text.Json;

namespace NekoliteUpdater.Repositories
{
    public class GitlabPackageStore : ICloudPackageStore
    {
        private readonly HttpClient client = new();
        private readonly JsonSerializerOptions options = new() { PropertyNamingPolicy = SnakeCaseNamingPolicy.Instance };

        public async Task<IReleaseVersion?> GetLatestVersionAsync(string? url, CancellationToken token)
        {
            using var stream = await client.GetStreamAsync(url, token);
            var versions = await JsonSerializer.DeserializeAsync<Release[]>(stream, options, token);
            var last = versions?.OrderBy(x => x.ReleasedAt).LastOrDefault();
            return last is null ? null : new ReleaseVersion(last.Name, last.TagName, last.ReleasedAt, last.Assets.Links[0].Url);
        }

        public async Task<Stream> GetReleaseStreamAsync(IReleaseVersion version, CancellationToken token)
        {
            return await client.GetStreamAsync(version.PackageUrl, token);
        }

        private record ReleaseVersion(string Name, string Version, DateTime ReleaseDate, string PackageUrl) : IReleaseVersion
        {
        }

        private record Release(string Name, string TagName, DateTime ReleasedAt, Assets Assets);
        private record Assets(Link[] Links);
        private record Link(string Url);

        private class SnakeCaseNamingPolicy : JsonNamingPolicy
        {
            public static SnakeCaseNamingPolicy Instance { get; } = new();

            public override string ConvertName(string name)
            {
                return name.ToSnakeCase();
            }
        }

    }
}

