﻿using System.Diagnostics;
using System.Runtime.Versioning;

namespace NekoliteUpdater.Repositories
{
    [SupportedOSPlatform("Windows")]
    internal class ServiceManager : IServiceManager
    {

        public async Task StopServiceAsync(CancellationToken token)
        {
            await Process.Start("cmd", "/c sc stop NekoliteWorker").WaitForExitAsync(token);
            await Process.Start("cmd", "/c iisreset /stop").WaitForExitAsync(token);
        }

        public async Task StartServiceAsync(CancellationToken token)
        {
            await Process.Start("cmd", "/c sc start NekoliteWorker").WaitForExitAsync(token);
            await Process.Start("cmd", "/c iisreset /start").WaitForExitAsync(token);
        }
    }
}

