﻿using Microsoft.Extensions.Options;
using NekoliteUpdater.Models;
using System.Diagnostics;
using System.IO.Compression;
using System.Reflection;

namespace NekoliteUpdater.Repositories
{
    public class LocalPackageStore : ILocalPackageStore
    {
        private readonly ApplicationSettings settings;

        public LocalPackageStore(IOptions<ApplicationSettings> options)
        {
            settings = options.Value;
        }

        public bool ExistUpdate()
        {
            var root = settings.ApplicationRoot;

            return Directory.EnumerateDirectories(root)
                .Where(x => x.ToLowerInvariant().EndsWith("update"))
                .SelectMany(x => Directory.EnumerateFiles(x, "Nekolite*.dll", SearchOption.AllDirectories))
                .Any();
        }

        public Task<string?> GetVersionAsync(CancellationToken token)
        {
            try
            {
                var root = settings.ApplicationRoot;
                var file = Directory.EnumerateDirectories(root)
                    .Where(x => !x.ToLowerInvariant().EndsWith("backup"))
                    .Where(x => !x.ToLowerInvariant().EndsWith("update"))
                    .SelectMany(x => Directory.EnumerateFiles(x, "Nekolite*.dll", SearchOption.AllDirectories))
                    .First();
                var version = FileVersionInfo.GetVersionInfo(file);
                return Task.FromResult(version.ProductVersion);
            }
            catch
            {
                var version = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);
                return Task.FromResult(version.ProductVersion);
            }
        }

        public async Task ExtractAsync(Stream stream, CancellationToken token)
        {
            var root = settings.ApplicationRoot;
            var update = Path.Combine(root, "update");
            try
            {
                await Task.CompletedTask;
                using var zipfile = new ZipArchive(stream, ZipArchiveMode.Read);
                zipfile.ExtractToDirectory(update, true);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                Directory.Delete(update, true);
            }
        }

        public ValueTask OverwriteAsync(CancellationToken token)
        {
            return OverwriteAsync(0, token);
        }

        public async ValueTask OverwriteAsync(int tryCount, CancellationToken token)
        {
            try
            {
                var root = settings.ApplicationRoot;
                var backup = Path.Combine(root, "backup");
                var update = Path.Combine(root, "update", "bin");
                Directory.CreateDirectory(backup).EnumerateFiles()
                    .ToList().ForEach(x => x.Delete());
                Directory.CreateDirectory(backup).EnumerateDirectories()
                    .ToList().ForEach(x => x.Delete(true));
                Directory.EnumerateDirectories(root)
                    .Where(x => !x.ToLowerInvariant().EndsWith("backup"))
                    .Where(x => !x.ToLowerInvariant().EndsWith("update"))
                    .Select(x => (from: x, to: Path.Combine(backup, Path.GetFileName(x))))
                    .ToList().ForEach(x => Directory.Move(x.from, x.to));

                Directory.EnumerateDirectories(update)
                    .Select(x => (from: x, to: Path.Combine(root, Path.GetFileName(x))))
                    .ToList().ForEach(x => Directory.Move(x.from, x.to));
            }
            catch
            {
                if (tryCount > 10) throw;
                await Task.Delay(1000, default);
                await OverwriteAsync(tryCount + 1, token);
            }
        }
    }
}

