﻿using NekoliteUpdater.Repositories;

namespace NekoliteUpdater.UseCases
{
    public class Updater
    {
        private readonly ILogger<Updater> logger;
        private readonly ICloudPackageStore cloud;
        private readonly ILocalPackageStore local;
        private readonly IServiceManager manager;

        public Updater(ILogger<Updater> logger, IServiceManager manager, ICloudPackageStore cloud, ILocalPackageStore local)
        {
            this.logger = logger;
            this.manager = manager;
            this.cloud = cloud;
            this.local = local;
        }

        public async Task ExecuteAsync(string? url, CancellationToken token = default)
        {
            logger.LogInformation("Checking Version...");
            var version = await cloud.GetLatestVersionAsync(url, token);
            var localVer = await local.GetVersionAsync(token);
            logger.LogInformation("{url} -> {Version} (Released At: {ReleaseDate})", url, version?.Version, version?.ReleaseDate);
            if (version is not null && version.Version != localVer)
            {
                if (!local.ExistUpdate())
                {
                    logger.LogInformation("Downloading Package...");
                    using var stream = await cloud.GetReleaseStreamAsync(version, token);
                    await local.ExtractAsync(stream, token);
                }
                logger.LogInformation("Stopping Service...");
                await manager.StopServiceAsync(token);
                logger.LogInformation("Updating Package...");
                await local.OverwriteAsync(token);
                await Task.Delay(1000);
                logger.LogInformation("Starting Service...");
                await manager.StartServiceAsync(token);
                logger.LogInformation("Update Completed!");
            }
            else
            {
                logger.LogInformation("Version up to date (Version: {Version})", localVer);
            }
        }
    }
}

