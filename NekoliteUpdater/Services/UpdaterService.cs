﻿using Microsoft.Extensions.Options;
using NekoliteUpdater.Models;
using NekoliteUpdater.UseCases;

namespace NekoliteUpdater.Services
{
    public class UpdaterService : BackgroundService
    {
        private readonly Updater updater;
        private readonly ILogger<UpdaterService> logger;
        private readonly ApplicationSettings settings;
        public UpdaterService(ILogger<UpdaterService> logger, Updater updater, IOptions<ApplicationSettings> options)
        {
            this.logger = logger;
            this.updater = updater;
            settings = options.Value;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    await updater.ExecuteAsync(settings.Url, stoppingToken);
                    await Task.Delay(TimeSpan.FromMinutes(settings.CheckCycle), stoppingToken);
                }
                catch (Exception e)
                {
                    logger.LogInformation("{Message} \n{StackTrace}", e.Message, e.StackTrace);
                }
            }
        }
    }
}

