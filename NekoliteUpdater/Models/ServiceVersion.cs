﻿namespace NekoliteUpdater.Models
{
    public interface IReleaseVersion
    {
        string Name { get; }
        string Version { get; }
        string PackageUrl { get; }
        DateTime ReleaseDate { get; }
    }
}

