﻿namespace NekoliteUpdater.Models
{
    public class ApplicationSettings
    {
        public string Url { get; init; } = default!;
        public double CheckCycle { get; init; }
        public string ApplicationRoot { get; init; } = default!;
    }
}

