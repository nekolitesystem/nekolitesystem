﻿using NekoliteUpdater.Models;
using NekoliteUpdater.Repositories;
using NekoliteUpdater.Services;
using NekoliteUpdater.UseCases;
using System.Runtime.InteropServices;

IHost host = Host.CreateDefaultBuilder(args)
    .UseWindowsService(options => options.ServiceName = "NekoliteUpdater")
    .ConfigureServices((builder, services) =>
    {
        services.Configure<ApplicationSettings>(builder.Configuration.GetSection("ApplicationSettings"));
        services.AddSingleton<ILocalPackageStore, LocalPackageStore>();
        services.AddSingleton<ICloudPackageStore, GitlabPackageStore>();
        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
        {
            services.AddSingleton<IServiceManager, ServiceManager>();
        }
        else
        {
            services.AddSingleton<IServiceManager, EmptyServiceManager>();
        }
        services.AddSingleton<Updater>();
        services.AddHostedService<UpdaterService>();
    })
    .Build();


await host.RunAsync();

