﻿namespace NekoliteCore.SensorModule
{
    public interface ISensorModuleFactory
    {
        ISensorModule CreateModule(string portName);
    }
}
