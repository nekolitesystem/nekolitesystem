﻿namespace NekoliteCore.SensorModule.Models
{
    public record SensorData(string Status, Meter Speed, Meter Tacho, Temps Tempertures, Vector Accel, Vector Angle, Vector Euler, Compass Rotate, Voltage Voltage, bool Shutdown)
    {

        public static SensorData Default => new(State.Initial, Meter, Meter, Temps, Vector, Vector, Vector, Compass, Volt, false);
        private static Meter Meter => new("", 0);
        private static Temps Temps => new("", 0, 0, 0, 0);
        private static Vector Vector => new(0, 0, 0);
        private static Voltage Volt => new(0);
        private static Compass Compass => new(0);

        public static implicit operator SensorData?(string? line)
        {
            var args = line?.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            //File.AppendAllText("calib.csv", string.Join(",", args!) + "\n");
            if (args is null || args.Length <= 16) { return null; }
            Meter speed = new("kph", args[0].ToDouble(0));
            Meter tacho = new("rpm", args[1].ToDouble(0));
            Temps temps = new("°C", args[2].ToDouble(0), args[3].ToDouble(0), args[4].ToDouble(0), args[5].ToDouble(0));
            Vector acc = new(args[07].ToDouble(0), args[08].ToDouble(0), args[09].ToDouble(0));
            Vector rot = new(args[10].ToDouble(0), args[11].ToDouble(0), args[12].ToDouble(0));
            Vector mag = new(args[13].ToDouble(0), args[14].ToDouble(0), args[15].ToDouble(0));
            Compass com = new(mag.Z);
            Voltage volt = new(args[16].ToDouble(0));
            bool shutdown = args[17] == "1";
            return new(State.Receive, speed, tacho, temps, acc, rot, mag, com, volt, shutdown);
        }
    }

    public static class State
    {
        public const string Initial = nameof(Initial);
        public const string Found = nameof(Found);
        public const string Receive = nameof(Receive);
        public const string NotFound = nameof(NotFound);
    }

    public record Meter(string Unit, double Value);

    public record Temps(string Unit, double Temp1, double Temp2, double Temp3, double Temp4);

    public record Vector(double X, double Y, double Z);

    public record Compass(double Roll);

    public record Voltage(double V1);
}
