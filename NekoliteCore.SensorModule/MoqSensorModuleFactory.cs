﻿namespace NekoliteCore.SensorModule
{
    internal class MoqSensorModuleFactory : ISensorModuleFactory
    {
        public ISensorModule CreateModule(string portName)
        {
            return new MoqSensorModule();
        }
    }
}
