﻿using NekoliteCore.SensorModule.Models;

namespace NekoliteCore.SensorModule
{
    public interface ISensorModule
    {
        IObservable<SensorData> GetObservable();
    }
}
