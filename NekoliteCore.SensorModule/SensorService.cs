﻿using System.Collections.Concurrent;

namespace NekoliteCore.SensorModule
{
    public class SensorService
    {
        private readonly ConcurrentDictionary<string, ISensorModule> modules = new();

        private readonly ISensorModuleFactory factory;

        public SensorService(ISensorModuleFactory factory)
        {
            this.factory = factory;
        }

        public ISensorModule GetOrCreate(string portName)
        {
            lock (this)
            {
                return modules.GetOrAdd(portName, factory.CreateModule);
            }
        }
    }
}
