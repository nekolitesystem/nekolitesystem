﻿using NekorhiteCore.Sensor.Models;
using NekorhiteCore.Sensor.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NekorhiteCore.Sensor.Implements.GrpcService
{
    internal class GrpcSensorModuleFactory : ISensorModuleFactory
    {
        private readonly Proto.Sensor.SensorClient client;
        public GrpcSensorModuleFactory(Proto.Sensor.SensorClient client)
        {
            this.client = client;
        }

        public ISensorModule CreateModule(string portName)
        {
            return new GrpcSensorModule(client);
        }
    }
}
