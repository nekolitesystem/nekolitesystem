﻿namespace NekoliteCore.SensorModule.SerialPort
{
    internal class SensorModuleFactory : ISensorModuleFactory
    {
        public ISensorModule CreateModule(string portName)
        {
            return new SensorModule(portName);
        }
    }
}
