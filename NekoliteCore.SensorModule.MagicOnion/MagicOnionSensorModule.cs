﻿using Grpc.Core;
using Grpc.Net.Client;
using MagicOnion.Client;
using NekoliteCore.Messages;
using NekoliteCore.SensorModule.Models;
using System.Reactive.Linq;

namespace NekoliteCore.SensorModule.MagicOnion
{
    internal class MagicOnionSensorModule : ISensorModule, IDisposable
    {
        private readonly GrpcChannel channel;
        private readonly ISensorModuleService client;

        private event EventHandler<SensorData>? DataReceived;

        public MagicOnionSensorModule(string portName)
        {
            channel = GrpcChannel.ForAddress(portName, new() { Credentials = ChannelCredentials.Insecure });
            client = MagicOnionClient.Create<ISensorModuleService>(channel);
        }

        public IObservable<SensorData> GetObservable()
        {
            var source = new CancellationTokenSource();
            Task.Factory.StartNew(ReceiveData, source.Token, TaskCreationOptions.LongRunning);
            return Observable.FromEventPattern<SensorData>(x => DataReceived += x, x => DataReceived -= x)
                .Select(x => x.EventArgs)
                .Prepend(SensorData.Default with { Status = State.Found })
                .Finally(source.Cancel)
                .Finally(source.Dispose);
        }

        private async Task ReceiveData(object? cancellationToken)
        {
            using var streaming = await client.StartSensorStreaming();
            var reader = streaming.ResponseStream;
            var token = (CancellationToken)cancellationToken!;
            while (await reader.MoveNext(token))
            {
                var temp = reader.Current.Status.Temperture;
                var acc = reader.Current.Motion.Accelerometer;
                var rot = reader.Current.Motion.Gyroscope;
                var mag = reader.Current.Motion.Magnetometer;
                var data = SensorData.Default with
                {
                    Status = State.Receive,
                    Speed = new("km/h", reader.Current.Status.Speed),
                    Tacho = new("rpm", reader.Current.Status.Tacho),
                    Tempertures = new("°C", temp.T1, temp.T2, temp.T3, temp.T4),
                    Accel = new(acc.X, acc.Y, acc.Z),
                    Angle = new(rot.X, rot.Y, rot.Z),
                    Euler = new(mag.X, mag.Y, mag.Z),
                    Rotate = new(reader.Current.Compass),
                    Voltage = new(reader.Current.Status.Voltage),
                };
                DataReceived?.Invoke(this, data);
            }
        }

        public void Dispose()
        {
            channel.Dispose();
        }
    }
}
