﻿using Microsoft.Extensions.DependencyInjection;

namespace NekoliteCore.SensorModule.MagicOnion
{
    public static partial class Extensions
    {

        public static IServiceCollection UseGrpcSensor(this IServiceCollection service)
        {
            return service
                .AddSingleton<SensorService>()
                .AddTransient<ISensorModuleFactory, MagicOnionSensorModuleFactory>();
        }
    }
}
