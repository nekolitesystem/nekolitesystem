﻿namespace NekoliteCore.SensorModule.MagicOnion
{
    internal class MagicOnionSensorModuleFactory : ISensorModuleFactory
    {
        public MagicOnionSensorModuleFactory()
        {
        }

        public ISensorModule CreateModule(string portName)
        {
            return new MagicOnionSensorModule(portName);
        }
    }
}
