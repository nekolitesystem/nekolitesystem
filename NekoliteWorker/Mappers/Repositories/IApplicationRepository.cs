﻿using NekorhiteWorker.Models;

namespace NekorhiteWorker.Mappers.Repositories
{
    public interface IApplicationRepository
    {
        Task<INekorhiteSettings> GetSettingsAsync();

        Task SetSettingsAsync(INekorhiteSettings settings);
    }
}
