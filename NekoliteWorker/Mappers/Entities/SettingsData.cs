﻿using NekorhiteWorker.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace NekorhiteWorker.Mappers.Entities
{
    public class SettingsData : INekorhiteSettings
    {
        public Guid Id { get; set; }

        public string? Title { get; set; }

        [Column("Calib_MagnetX")]
        public double MagX { get; set; }

        [Column("Calib_MagnetY")]
        public double MagY { get; set; }

        [Column("Calib_MagnetZ")]
        public double MagZ { get; set; }
    }
}
