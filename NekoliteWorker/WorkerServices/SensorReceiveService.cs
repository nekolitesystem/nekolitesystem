﻿using Grpc.Core;
using Grpc.Net.Client;
using MagicOnion.Client;
using Microsoft.Extensions.Options;
using NekoliteCore.Messages;
using NekoliteCore.SensorModule;
using NekoliteCore.SensorModule.Models;
using NekorhiteWorker.Configurations;
using System.Diagnostics;

namespace NekorhiteWorker.Services
{
    public class SensorReceiveService : BackgroundService
    {
        private readonly ILoggerFactory logger;
        private readonly SensorSettings settings;
        private readonly SensorService service;

        private readonly int port;

        private SensorData last = SensorData.Default;

        public SensorReceiveService(ILoggerFactory logger, IOptions<SensorSettings> settings, IConfiguration config, SensorService service)
        {
            this.settings = settings.Value;
            this.logger = logger;
            this.service = service;
            port = new Uri(config.GetSection("Kestrel").GetSection("Endpoints").GetSection("Http").GetValue<string>("Url")).Port;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            logger.CreateLogger<SensorReceiveService>().LogInformation("車載センサー監視サービスを起動しました。");
            return base.StartAsync(cancellationToken);
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            logger.CreateLogger<SensorReceiveService>().LogInformation("車載センサー監視サービスを停止しました。");
            return base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                using var channel = GrpcChannel.ForAddress($"http://localhost:{port}", new() { Credentials = ChannelCredentials.Insecure });
                var client = MagicOnionClient.Create<ISensorModuleService>(channel);
                var sensor = service.GetOrCreate(settings.PortName);
                using var subscribe = sensor.GetObservable().Subscribe(data => SendData(client, data));

                while (!stoppingToken.IsCancellationRequested)
                {
                    await Task.Delay(1000, stoppingToken);
                    if (last.Shutdown)
                    {
                        Process.Start("shutdown.exe", "/S /t 0");
                    }
                }
            }
            catch
            {
            }
        }

        private void SendData(ISensorModuleService client, SensorData data)
        {
            last = data;
            if (settings.Acceleration && settings.Gyroscope && settings.Magnetism)
            {
                var accel = new Vec3d(data.Accel.X, data.Accel.Y, data.Accel.Z);
                var angle = new Vec3d(data.Angle.X, data.Angle.Y, data.Angle.Z);
                var euler = new Vec3d(data.Euler.X, data.Euler.Y, data.Euler.Z);
                client.SendMotionDataAsync(new(accel, angle, euler));
            }
            else
            {
                if (settings.Acceleration)
                {
                    client.SendAccelerometerAsync(new(data.Accel.X, data.Accel.Y, data.Accel.Z));
                }
                if (settings.Gyroscope)
                {
                    client.SendGyroscopeAsync(new(data.Angle.X, data.Angle.Y, data.Angle.Z));
                }
                if (settings.Magnetism)
                {
                    client.SendMagnetometerAsync(new(data.Euler.X, data.Euler.Y, data.Euler.Z));
                }
            }
            if (settings.Compass)
            {
                client.SendCompassAsync(Math.Atan2(data.Euler.X, data.Euler.Y) * 180 / Math.PI);
            }
            var temp = data.Tempertures;
            client.SendStatusAsync(new(new(temp.Temp1, temp.Temp2, temp.Temp3, temp.Temp4), data.Speed.Value, data.Tacho.Value, data.Voltage.V1));
        }
    }
}
