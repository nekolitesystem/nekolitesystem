﻿namespace NekorhiteWorker.Configurations
{
    public record SensorSettings
    {
        public string PortName { get; set; } = default!;
        public bool Acceleration { get; set; }
        public bool Gyroscope { get; set; }
        public bool Magnetism { get; set; }
        public bool Compass { get; set; }
    }
}
