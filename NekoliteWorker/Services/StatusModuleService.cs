﻿using MagicOnion;
using MagicOnion.Server;
using NekoliteCore.MachineStatus.Services;
using NekoliteWorker.Services.Status;

namespace NekorhiteWorker.Services
{
    public class StatusModuleService : ServiceBase<IStatusModuleService>, IStatusModuleService
    {
        private static readonly IStatusService status;

        static StatusModuleService()
        {
            status = OperatingSystem.IsWindows() ?
                new WindowsStatusService() : new DummyStatusService();
        }

        public UnaryResult<StatusData> GetStatusAsync()
        {
            var state = status.GetStatus();
            return UnaryResult<StatusData>(new(state.CpuLoad, state.MemoryUsed, state.Temperature));
        }
    }
}
