﻿using MagicOnion;
using MagicOnion.Server;
using NekoliteCore.Messages;
using NekorhiteWorker.Mappers.Repositories;
using NekorhiteWorker.Models;

namespace NekorhiteWorker.Services
{
    public class NekorhiteService : ServiceBase<INekoliteService>, INekoliteService
    {
        private readonly IApplicationRepository repository;

        public NekorhiteService(IApplicationRepository repository)
        {
            this.repository = repository;
        }

        public async UnaryResult<NekoliteSettings> GetSettingsAsync()
        {
            var settings = await repository.GetSettingsAsync();
            return new(settings.Title, settings.MagX, settings.MagY, settings.MagZ);
        }

        public async UnaryResult<NekoliteSettings> SetSettingsAsync(NekoliteSettings settings)
        {
            var convertion = new ConvertionNekorhiteSetting(settings);
            await repository.SetSettingsAsync(convertion);
            return convertion;
        }

        private record ConvertionNekorhiteSetting : NekoliteSettings, INekorhiteSettings
        {
            public ConvertionNekorhiteSetting(NekoliteSettings neko) : base(neko.Title, neko.MagX, neko.MagY, neko.MagZ) { }
        }
    }
}
