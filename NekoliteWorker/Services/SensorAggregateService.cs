﻿using MagicOnion;
using MagicOnion.Server;
using Microsoft.Extensions.Options;
using NekoliteCore.Messages;
using NekorhiteWorker.Configurations;
using NekorhiteWorker.Mappers.Repositories;
using System.Reactive.Linq;

namespace NekorhiteWorker.Services
{
    public class SensorAggregateService : ServiceBase<ISensorModuleService>, ISensorModuleService
    {
        private static event EventHandler? SensorUpdateEvent;

        private static Vec3d acc = new(0, 0, 0);
        private static Vec3d mag = new(0, 0, 0);
        private static Vec3d rot = new(0, 0, 0);
        private static double compass;

        private static Temp temp = new(0, 0, 0, 0);
        private static double speed;
        private static double tacho;
        private static double voltage;

        private static SensorResponse SensorData => new(new(acc, rot, mag), compass, new(temp, speed, tacho, voltage));

        private readonly SensorSettings settings;

        private readonly IApplicationRepository repository;

        public SensorAggregateService(IOptions<SensorSettings> settings, IApplicationRepository repository)
        {
            this.settings = settings.Value;
            this.repository = repository;
        }

        public async Task<ServerStreamingResult<SensorResponse>> StartSensorStreaming()
        {
            var context = GetServerStreamingContext<SensorResponse>();
            var observable = Observable.FromEventPattern(x => SensorUpdateEvent += x, x => SensorUpdateEvent -= x);
            using var subscriver = observable.Subscribe();
            foreach (var data in observable.Select(x => SensorData))
            {
                await context.WriteAsync(data);
            }
            return context.Result();
        }

        public UnaryResult<SensorResponse> SendMotionDataAsync(MotionData motion)
        {
            acc = motion.Accelerometer;
            mag = motion.Magnetometer;
            rot = motion.Gyroscope;
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorResponse> SendStatusAsync(VehicleStatus status)
        {
            temp = status.Temperture;
            speed = status.Speed;
            tacho = status.Tacho;
            voltage = status.Voltage;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorResponse> SendAccelerometerAsync(Vec3d vector)
        {
            acc = vector;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorResponse> SendGyroscopeAsync(Vec3d vector)
        {
            rot = vector;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorResponse> SendMagnetometerAsync(Vec3d vector)
        {
            mag = vector;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorResponse> SendCompassAsync(double rotate)
        {
            compass = rotate;
            SensorUpdateEvent?.Invoke(this, EventArgs.Empty);
            return UnaryResult(SensorData);
        }

        public UnaryResult<SensorSettingData> GetSettingsAsync()
        {
            var ret = settings;
            return UnaryResult(new SensorSettingData(ret.Acceleration, ret.Gyroscope, ret.Magnetism, ret.Compass));
        }

    }
}
