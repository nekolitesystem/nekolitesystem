﻿namespace NekoliteWorker.Services.Status
{
    public class DummyStatusService : IStatusService
    {
        public Status GetStatus()
        {
            return new(0, 0, 0);
        }
    }
}

