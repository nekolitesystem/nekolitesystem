﻿using System.Diagnostics;
using System.Management;
using System.Runtime.Versioning;

namespace NekoliteWorker.Services.Status
{
    [SupportedOSPlatform("Windows")]
    public class WindowsStatusService : IStatusService
    {
        private readonly List<ManagementObjectSearcher> cpu = new();
        private readonly List<ManagementObjectSearcher> mem = new();
        private readonly List<PerformanceCounter> tmps = new();

        public WindowsStatusService()
        {
            try
            {
                var names = new PerformanceCounterCategory("Thermal Zone Information").GetInstanceNames();
                tmps.AddRange(names.Select(x => new PerformanceCounter("Thermal Zone Information", "Temperature", x)));
                cpu.Add(new(new("\\\\.\\ROOT\\cimv2"), new ObjectQuery("SELECT * FROM Win32_PerfFormattedData_PerfOS_Processor")));
                mem.Add(new(new("\\\\.\\ROOT\\cimv2"), new ObjectQuery("SELECT * FROM Win32_PerfFormattedData_PerfOS_Memory")));
            }
            catch
            {

            }
        }

        public Status GetStatus()
        {
            var cpuobj = cpu.SelectMany(x => x.Get().OfType<ManagementObject>()).FirstOrDefault(x => $"{x["Name"]}" == "_Total");
            var cpuval = (ulong?)cpuobj?["PercentProcessorTime"] ?? 0;

            var memobj = mem.SelectMany(x => x.Get().OfType<ManagementObject>()).FirstOrDefault();
            var memval = (uint?)memobj?["PercentCommittedBytesInUse"] ?? 0;

            var tmpval = tmps.Select(x => (double)x.NextValue() - 273).FirstOrDefault();
            return new(cpuval, memval, tmpval);
        }
    }
}

