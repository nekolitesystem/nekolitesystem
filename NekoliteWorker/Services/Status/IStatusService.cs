﻿namespace NekoliteWorker.Services.Status
{
    public interface IStatusService
    {
        Status GetStatus();
    }

    public record Status(double CpuLoad, double MemoryUsed, double Temperature);
}

