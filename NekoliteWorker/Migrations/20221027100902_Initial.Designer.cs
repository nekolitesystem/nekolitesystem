﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using NekorhiteWorker.Mappers;

#nullable disable

namespace NekorhiteWorker.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20221027100902_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "6.0.9");

            modelBuilder.Entity("NekorhiteWorker.Mappers.Entities.SettingsData", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<double>("Calib_MagnetX")
                        .HasColumnType("REAL");

                    b.Property<double>("Calib_MagnetY")
                        .HasColumnType("REAL");

                    b.Property<double>("Calib_MagnetZ")
                        .HasColumnType("REAL");

                    b.Property<string>("Title")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Settings");
                });
#pragma warning restore 612, 618
        }
    }
}
