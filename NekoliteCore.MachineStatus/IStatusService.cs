﻿namespace NekoliteCore.MachineStatus
{
    public interface IStatusService
    {
        Task<StatusModel> GetStatusAsync();
    }

    public record StatusModel(double CpuPercent, double MemPercent, double[] CpuTempertures);
}
