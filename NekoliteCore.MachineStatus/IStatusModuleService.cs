﻿using MagicOnion;
using MessagePack;

namespace NekoliteCore.MachineStatus.Services
{
    public interface IStatusModuleService : IService<IStatusModuleService>
    {
        UnaryResult<StatusData> GetStatusAsync();
    }

    [MessagePackObject(true)]
    public record StatusData(double CpuAverage, double MemoryUsed, double Temperature);
}
