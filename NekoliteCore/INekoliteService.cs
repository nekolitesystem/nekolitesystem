﻿using MagicOnion;
using MessagePack;

namespace NekoliteCore.Messages
{
    public interface INekoliteService : IService<INekoliteService>
    {

        UnaryResult<NekoliteSettings> GetSettingsAsync();

        UnaryResult<NekoliteSettings> SetSettingsAsync(NekoliteSettings settings);
    }

    [MessagePackObject(true)]
    public record NekoliteSettings(string? Title, double MagX, double MagY, double MagZ);
}
