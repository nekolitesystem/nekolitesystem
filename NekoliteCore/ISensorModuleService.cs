﻿using MagicOnion;
using MessagePack;

namespace NekoliteCore.Messages
{
    public interface ISensorModuleService : IService<ISensorModuleService>
    {

        Task<ServerStreamingResult<SensorResponse>> StartSensorStreaming();

        UnaryResult<SensorResponse> SendMotionDataAsync(MotionData motion);

        UnaryResult<SensorResponse> SendStatusAsync(VehicleStatus status);

        UnaryResult<SensorResponse> SendAccelerometerAsync(Vec3d vector);

        UnaryResult<SensorResponse> SendGyroscopeAsync(Vec3d vector);

        UnaryResult<SensorResponse> SendMagnetometerAsync(Vec3d vector);

        UnaryResult<SensorResponse> SendCompassAsync(double rotate);

        UnaryResult<SensorSettingData> GetSettingsAsync();
    }

    [MessagePackObject(true)]
    public record SensorResponse(MotionData Motion, double Compass, VehicleStatus Status);

    [MessagePackObject(true)]
    public record MotionData(Vec3d Accelerometer, Vec3d Gyroscope, Vec3d Magnetometer);

    [MessagePackObject(true)]
    public record VehicleStatus(Temp Temperture, double Speed, double Tacho, double Voltage);

    [MessagePackObject(true)]
    public record Vec3d(double X, double Y, double Z);

    [MessagePackObject(true)]
    public record Temp(double T1, double T2, double T3, double T4);

    [MessagePackObject(true)]
    public record SensorSettingData(bool Accelerometer, bool Gyroscope, bool Magnetometer, bool Compass);
}
