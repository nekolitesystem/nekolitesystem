﻿using Microsoft.AspNetCore.Mvc;
using NekoliteCore.MachineStatus;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Reflection;

namespace NekorhiteTourersSystem.Controllers
{
    [Route("info")]
    [ApiController]
    public class InfomationController : ControllerBase
    {
        private static readonly string path = Assembly.GetExecutingAssembly().Location;
        private static readonly string version = FileVersionInfo.GetVersionInfo(path).ProductVersion ?? "";
        private readonly IStatusService service;

        public InfomationController(IStatusService service)
        {
            this.service = service;
        }

        [HttpGet("locale")]
        public IActionResult Locale()
        {
            return Ok(new { Locale = Request.Headers.TryGetValue("Accept-Language", out var a) ? a.FirstOrDefault() : "en" });
        }

        [HttpGet("status")]
        public async Task<StatusModel> GetServerStatus()
        {
            var status = await service.GetStatusAsync();
            return new(status.CpuPercent, status.MemPercent, status.CpuTempertures, version);
        }

        [HttpGet("address")]
        public AddressModel Address()
        {
            return new(NetworkInterface.GetAllNetworkInterfaces()
               .Where(x => x.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
               .Where(x => x.OperationalStatus == OperationalStatus.Up)
               .SelectMany(x => x.GetIPProperties().UnicastAddresses)
               .Where(x => x.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
               .Select(x => x.Address.ToString()).Prepend(HttpContext.Connection.LocalIpAddress!.ToString()).Distinct());
        }

        public record StatusModel(double CpuAverage, double MemoryUsed, double[] CpuTempertures, string Version);

        public record AddressModel(IEnumerable<string> AddressList);
    }
}
