const { env } = require('process');

const target = env.ASPNETCORE_HTTPS_PORT ? `https://localhost:${env.ASPNETCORE_HTTPS_PORT}` :
  env.ASPNETCORE_URLS ? env.ASPNETCORE_URLS.split(';')[0] : 'http://localhost:51812';

const PROXY_CONFIG = [
  {
    context: [
      "/api/**",
      "/live/**",
      "/info/**"
   ],
    target: target,
    secure: false
  },
  {
    context: [
      "/api/status/**",
    ],
    target: target.replace("http", "ws"),
    secure: false,
    ws: true
  },
]

module.exports = PROXY_CONFIG;
