import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Awaitable, SensorService, Vector3d } from '../sensor/sensor.service';

@Component({
  selector: 'carib-control',
  templateUrl: './caribration.component.html',
  styleUrls: [ './caribration.component.scss' ]
})
export class CaribrationComponent {
  isVisible = false;
  isEnable = false;

  x_max: number | undefined;
  y_max: number | undefined;
  z_max: number | undefined;

  x_min: number | undefined;
  y_min: number | undefined;
  z_min: number | undefined;

  a = 0;
  b = 0;

  get x() { return this.sum(this.x_max, this.x_min) / 2; }
  get y() { return this.sum(this.y_max, this.y_min) / 2; }
  get z() { return this.sum(this.z_max, this.z_min) / 2; }

  constructor(private sensor: SensorService) {
    sensor.orientations.subscribe(this.onNext)
  }

  async ngOnInit() {
    while (true) {
      if (this.isVisible != this.sensor.isVisible) {
        if (this.isVisible = this.sensor.isVisible) {
          this.resetCaribration()
        }
      }
      await Awaitable.delay(30)
    }
  }

  onNext = (data: Vector3d) => {
    if (this.isEnable) {
      this.x_max = Math.max(this.x_max ?? data.x, data.x)
      this.x_min = Math.min(this.x_min ?? data.x, data.x)
      this.y_max = Math.max(this.y_max ?? data.y, data.y)
      this.y_min = Math.min(this.y_min ?? data.y, data.y)
      this.z_max = Math.max(this.z_max ?? data.z, data.z)
      this.z_min = Math.min(this.z_min ?? data.z, data.z)

      const canvas = document.getElementById("calib_canvas")! as HTMLCanvasElement
      const context = canvas.getContext("2d")!
      context.beginPath();
      context.moveTo(this.a * 5 + 500, this.b * 5 + 500)
      context.lineTo(data.x * 5 + 500, data.y * 5 + 500)
      context.strokeStyle = "orange"
      context.lineWidth = 3
      context.stroke()
      context.save()
      canvas.style.left = `${50 - this.x / 2}%`
      canvas.style.top = `${50 - this.y / 2}%`
    }

    this.a = data.x;
    this.b = data.y;
  }

  startCaribration = () => {
    this.isEnable = true
    this.resetCaribration()
  }

  resetCaribration() {
    const canvas = document.getElementById("calib_canvas") as HTMLCanvasElement
    const context = canvas.getContext("2d")
    context?.clearRect(0, 0, 1000, 1000)
    this.x_max = this.x_min = undefined
    this.y_max = this.y_min = undefined
    this.z_max = this.z_min = undefined
  }

  async saveCaribration() {
    await this.sensor.setCaribration({ x: this.x, y: this.y, z: this.z })
    this.closeCaribration();
  }

  closeCaribration() {
    this.isEnable = false
    this.sensor.isVisible = false
  }

  sum(x = 0, y = 0) {
    return x + y
  }

}
