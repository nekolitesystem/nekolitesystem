import { Component } from '@angular/core';
import { ControlBase } from '../control.base';
import { Awaitable, SensorData } from '../sensor.service';

@Component({
  selector: 'tachometer-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class TachoMeterControl extends ControlBase {

  async onInitialized() {
    const indicator = document.getElementById("tachoindicator")!;
    await Awaitable.delay(2500);
    this.setValue(6000);
    indicator.style.transition = `transform 0.5s ease`;
    await Awaitable.delay(1000);
    this.setValue(0);
    indicator.style.transition = `transform 0.5s ease`;
  }

  onNext(data: SensorData) {
    this.setValue(data.tacho.value);
  }

  setValue(value: number) {
    const indicator = document.getElementById("tachoindicator")!;
    indicator.style.transform = `rotateZ(${Math.min(7000, value) / 50 - 110}deg)`;
    indicator.style.transition = `transform 0.25s ease`;
  }
}
