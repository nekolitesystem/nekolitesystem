import { Component, OnInit } from '@angular/core';
import { ControlBase } from '../control.base';
import { Awaitable, SensorData, SensorService, Vector3d } from '../sensor.service';

@Component({
  selector: 'accelerate-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class AccelerateControl extends ControlBase {
  private calx: number = 0;
  private accx: number = 0;
  private isFirst = true;

  async onInitialized() {
    const a = document.getElementById("accelerate+")!;
    const b = document.getElementById("accelerate-")!;

    a.style.transition = b.style.transition = `height 0.5s ease`;
    a.style.height = b.style.height = `000%`;
    await Awaitable.delay(2500);
    a.style.height = b.style.height = `100%`;
    await Awaitable.delay(1000);
    a.style.height = b.style.height = `000%`;
    await Awaitable.delay(500);
    a.style.transition = b.style.transition = `height 0s ease`;
  }

  onNext(data: SensorData) {
    const x = Math.sin(data.euler.y / 180 * Math.PI);
    const z = Math.cos(data.euler.y / 180 * Math.PI);
    const pitch = data.accel.x + data.accel.z / z * x;
    this.calx += (pitch - this.calx) / 1000;
    this.accx += (pitch - this.accx) / 5;
    this.setValue(this.accx - this.calx);
  }

  setValue(data: number) {
    const a = document.getElementById("accelerate+")!;
    const b = document.getElementById("accelerate-")!;
    a.style.height = `${Math.max(0,  data) * 20}%`;
    b.style.height = `${Math.max(0, -data) * 20}%`;
  }
}
