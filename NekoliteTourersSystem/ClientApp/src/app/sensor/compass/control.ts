import { Component, OnInit } from '@angular/core';
import { ControlBase } from '../control.base';
import { Awaitable, Compass, SensorData, SensorService, Vector3d } from '../sensor.service';

@Component({
  selector: 'compass-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class CompassControl extends ControlBase implements OnInit {
  magx = 100;
  magy = 0;
  rot = 0;

  calibx = 0;
  caliby = 0;
  calib = 0;
  constructor(private sensor: SensorService) {
    super(sensor)
  }

  async ngOnInit() {
    while (true) {
      const data = await this.sensor.getCaribration()
      this.calibx = data.x
      this.caliby = data.y
      await Awaitable.delay(1000);
    }
  }

  onNext(data: SensorData) {
    this.setValue(data.euler, data.rotate.roll);
  }

  setValue(data: Vector3d, roll: number) {
    const compass = document.getElementById("compasscenter")!;
    compass.style.transform = `rotateZ(${data.x}deg)`;
  }
}
