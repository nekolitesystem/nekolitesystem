import { Component, OnInit } from '@angular/core';
import { ControlBase } from '../control.base';
import { Awaitable, Compass, SensorData, SensorService, Vector3d } from '../sensor.service';

@Component({
  selector: 'rotation-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class RotationControl extends ControlBase implements OnInit {
  yaw = 0;
  dy = 0;
  roll = 0;
  pitch = 0;

  constructor(private sensor: SensorService) {
    super(sensor)
  }

  async ngOnInit() {

  }

  onNext(data: SensorData) {
    this.setValue(data.euler);
  }

  setValue(data: Vector3d) {
    this.roll = data.x;
    this.pitch = data.y;
    this.dy = this.yaw - data.z;
    this.yaw = data.z;
    const bank = document.getElementById("bank-ground")!;
    bank.style.transform = `rotateZ(${-this.roll}deg)`;
    const slope = document.getElementById("slope-ground")!;
    slope.style.transform = `rotateZ(${this.pitch}deg)`;
    const slope_body = document.getElementById("slope-body")!;
    slope_body.style.transform = `rotateZ(${this.pitch}deg)`;
    const rot = document.getElementById("rotate-meter")!;
    rot.style.transform = `rotateZ(${Math.max(Math.min(this.dy * 40, 45), -45)}deg)`;
  }
}
