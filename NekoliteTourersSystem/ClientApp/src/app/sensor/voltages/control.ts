import { Component } from '@angular/core';
import { ControlBase } from '../control.base';
import {  SensorData, Voltage } from '../sensor.service';

@Component({
  selector: 'voltages-control',
  templateUrl: './control.html',
  styleUrls: [ './control.scss' ]
})
export class VoltagesControl extends ControlBase {
  voltage = 0;

  onNext(data: SensorData) {
    this.setValue(data.voltage);
  }

  setValue(data: Voltage) {
    this.voltage = data.v1;
  }
}
