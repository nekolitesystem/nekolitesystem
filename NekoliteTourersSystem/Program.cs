using Grpc.Core;
using MagicOnion.Client;
using NekoliteCore.MachineStatus.Implements;
using NekoliteCore.Messages;
using NekoliteCore.SensorModule;
using NekoliteCore.SensorModule.MagicOnion;
using NekoliteCore.SensorModule.SerialPort;
using NekoliteCore.VideoStreaming;
using NekorhiteTourersSystem.Settings;

var builder = WebApplication.CreateBuilder(args);
var streamconfig = builder.Configuration.GetSection("StreamSettings");
var sensorconfig = builder.Configuration.GetSection("SensorSettings");
var statusconfig = builder.Configuration.GetSection("StatusSettings");
// Add services to the container.
builder.Services.AddControllersWithViews();

if (sensorconfig.GetValue<bool>("MoqModule"))
{
    builder.Services.UseMoqSensor();
}
else if (sensorconfig.GetValue<bool>("GrpcModule"))
{
    builder.Services.UseGrpcSensor();
}
else
{
    builder.Services.UseSensor();
}

builder.Services.AddGrpcClient<INekoliteService>(options =>
{
    options.Address = new Uri(statusconfig.GetValue<string>("Address"));
    options.ChannelOptionsActions.Add(x => x.Credentials = ChannelCredentials.Insecure);
    options.Creator = MagicOnionClient.Create<INekoliteService>;
});

builder.Services.EnableStreamingService();
builder.Services.UseRtspStreaming(streamconfig);
builder.Services.UseMachineStatus(statusconfig);
builder.Services.Configure<SensorSetting>(sensorconfig);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
}

app.UseStaticFiles();
app.UseRouting();

app.UseWebSockets();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapFallbackToFile("index.html"); ;

app.Run();
