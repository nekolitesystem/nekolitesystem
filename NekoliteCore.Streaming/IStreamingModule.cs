﻿namespace NekoliteCore.VideoStreaming
{
    public interface IStreamingModule
    {
        Task UpdateAsync(Stream stream);
    }
}
