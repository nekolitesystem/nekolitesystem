﻿namespace NekoliteCore.VideoStreaming
{
    public interface IStreamingModuleFactory
    {
        public IStreamingModule CreateModule(string key);
    }
}
