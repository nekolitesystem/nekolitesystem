﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NekoliteCore.VideoStreaming.OpenCV;

namespace NekoliteCore.VideoStreaming
{
    public static partial class Extensions
    {
        public static IServiceCollection UseRtspStreaming(this IServiceCollection service, IConfiguration configration)
        {
            return service
                .Configure<StreamingModuleConfig>(configration)
                .AddTransient<IStreamingModuleFactory, CaptureStreamingModuleFactory>();
        }
    }
}
