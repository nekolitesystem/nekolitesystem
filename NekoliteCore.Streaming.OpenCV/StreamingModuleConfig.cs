﻿namespace NekoliteCore.VideoStreaming.OpenCV
{
    public class StreamingModuleConfig
    {
        public ImageSize VideoSize { get; init; } = new();
        public ImageSize CaptureSize { get; init; } = new();
        public int Quality { get; init; }
        public bool Rtsp { get; init; }

        public int Device { get; init; }

        public string Address { get; init; } = string.Empty;
    }

    public class ImageSize
    {
        public int Width { get; init; }

        public int Height { get; init; }
    }

}
