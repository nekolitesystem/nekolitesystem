﻿using Microsoft.Extensions.Options;

namespace NekoliteCore.VideoStreaming.OpenCV
{
    internal class CaptureStreamingModuleFactory : IStreamingModuleFactory
    {
        public readonly StreamingModuleConfig config;

        public CaptureStreamingModuleFactory(IOptions<StreamingModuleConfig> options)
        {
            config = options.Value;
        }

        public IStreamingModule CreateModule(string _)
        {
            return new CaptureStreamingModule(config);
        }
    }
}
